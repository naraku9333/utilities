#ifndef SV_UTILITY_HPP
#define SV_UTILITY_HPP
#include <string>
#include <cstdint>

namespace sv
{
	namespace util
	{
		/**
		* Gets the current time as a std::string.
		*
		* @return std::string containing the current time
		*/
		std::string time_string();

		/**
		* Send get request for file from server
		* 
		* @param server ie: "www.google.com"
		* @param file to retrieve ie: "/search?q=irc+bot"
		* @return response data as string
		*
		* from http://www.boost.org/doc/libs/1_54_0/doc/html/boost_asio/example/cpp03/iostreams/http_client.cpp 
		*/
		std::string get_http_data(const std::string& server, const std::string& file);

		/**
		* Strip html encoding from a std::string.
		*
		* @param The string to strip of encoding
		*/
		void html_decode(std::string& str);

		/**
		* Retrieve weather data, 
		* requires api key from http://www.wunderground.com/weather/api/
		*
		* @param location to get weather for
		* @param api key
		* @return weather data as string
		*/
		std::string weather(const std::string& loc, const std::string& key);

		/**
		* Retrieve a cow say message from server
		*
		* @return vector containing cow say message
		*/
		const std::vector<std::string> cow_say();

		/**
		* Convert number in oldbase to base 10
		*
		* @param number as string
		* @param base converting from
		* @return base 10 number as uint64_t
		*/
		uint64_t sv::util::ToBaseTen(std::string num, int oldbase);

		/**
		* Convert base 10 number to base [2 - 36]
		*
		* @param number as string
		* @param base to convert to
		* @return converted number as string
		*/
		 std::string sv::util::FromBaseTen(std::string s, int newbase);
	}	
}
#endif